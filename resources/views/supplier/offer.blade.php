@extends('layouts.supplier')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Teikti pasiūlymą</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{action('SupplierController@makeOffer') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="item" class="col-md-4 control-label">Prekė</label>
                            
                            <div class="col-md-6">
                                <input id="item" type="text" class="form-control" name="item" value="{{old('item')}}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quantity" class="col-md-4 control-label">Kiekis</label>

                            <div class="col-md-6">
                                <input id="quantity" type="text" class="form-control" name="quantity" value="{{old('quantity')}}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-md-4 control-label">Vieneto kaina (eurais)</label>

                            <div class="col-md-6">
                                <input id="price" type="text" class="form-control" name="price" value="{{old('price')}}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Pateikti pasiūlymą
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
