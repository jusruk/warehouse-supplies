@extends('layouts.supplier')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            @if($offers->count() > 0)
                <div class="panel-heading">Pasiūlymai</div>
            @else
                <div class="panel-heading">Pasiūymų nėra</div>
            @endif
            @if($offers->count() > 0)  
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                          
                    <table class="table table-striped">
                    <tr>
                            <th>Prekė</th>
                            <th>Kiekis</th>
                            <th>Vieneto kaina</th>
                            <th>Trinti</th>
                        </tr>
                        @foreach($offers as $offer)
                            <tr>
                            <td>{{App\Item::where('id', '=', $offer->item_id)->first()->title}}</td>
                            <td>{{App\Item::where('id', '=', $offer->item_id)->first()->quantity}}</td>
                            <td>{{App\Item::where('id', '=', $offer->item_id)->first()->price}}</td>
                            <td><a class="btn btn-danger" href="{!!route('removeOffer', ['id' => $offer->id])!!}">Trinti</a></td>
                            </tr>
                        @endforeach
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
