@extends('layouts.admin') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Sistemos vartotojai</div>
				<div class="panel-body">
					@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
					@endif

					<table class="table table-striped">
					    <tr>
							<th>ID</th>
							<th>Vardas</th>
							<th>Elektroninis paštas</th>
							<th>Vartotojo rūšis</th>
						</tr>
						@foreach($users as $user)
						<tr>
							<td>{{$user['id']}}</td>
							<td>{{$user['name']}}</td>
							<td>{{$user['email']}}</td>
							<td>{{$user['role']}}</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection