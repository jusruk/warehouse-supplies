@extends('layouts.admin') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Prekių judėjimas</div>
				<div class="panel-body">
					@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
					@endif
					<table class="table table-striped">
						<tr>
							<th>Pavadinimas</th>
							<th>Vieneto kaina</th>
							<th>Kiekis</th>
							<th>Būsena</th>
							<th>Vadybininko duomenys</th>
						</tr>
						@foreach($items as $item)
						<tr>
							<td>{{$item->title}}</td>
							<td>{{$item->price}}</td>
							<td>{{$item->quantity}}</td>
							@php switch($item->location){ case 0: echo '
							<td>Siūloma</td>'; break; case 1: echo '
							<td>Sandėlyje</td>'; break; case 2: echo '
							<td>Parduota</td>'; break; } @endphp
							@if($item->location === 2)
								@foreach($managers as $manager)
									@if($manager['item'] == $item->id)
										<td>{{$manager['name']}} - {{$manager['email']}}</td>
										@break
									@endif
								@endforeach
							@else
							<td></td>
							@endif
						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection