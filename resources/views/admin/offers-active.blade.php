@extends('layouts.admin') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				@if(!empty($data))
				<div class="panel-heading">Aktyvūs pasiūlymai</div>
				@else
				<div class="panel-heading">Aktyvių pasiūlymų nėra</div>
				@endif
        @if(!empty($data))
				<div class="panel-body">
					@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
					@endif
					<table class="table table-striped">
						<tr>
							<th>Siūloma prekė</th>
							<th>Siūlomas kiekis</th>
							<th>Siūloma vieneto kaina</th>
						</tr>
						@foreach($data as $offer)
						<tr>
							<td>{{$offer['title']}}</td>
							<td>{{$offer['quantity']}}</td>
							<td>{{$offer['price']}}</td>
						</tr>
						@endforeach
					</table>
				</div>
        @endif
			</div>
		</div>
	</div>
</div>
@endsection