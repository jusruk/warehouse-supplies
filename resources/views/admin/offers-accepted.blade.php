@extends('layouts.admin') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				@if(!empty($data))
				<div class="panel-heading">Išnaudoti pasiūlymai</div>
				@else
				<div class="panel-heading">Išnaudotų pasiūlymų nėra</div>
				@endif
        @if(!empty($data))
				<div class="panel-body">
					@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
					@endif
					<table class="table table-striped">
						<tr>
							<th>Siūlyta prekė</th>
							<th>Siūlyta kiekis</th>
							<th>Siūlyta vieneto kaina</th>
						</tr>
						@foreach($data as $offer)
						<tr>
							<td>{{$offer['title']}}</td>
							<td>{{$offer['quantity']}}</td>
							<td>{{$offer['price']}}</td>
						</tr>
						@endforeach
					</table>
				</div>
        @endif
			</div>
		</div>
	</div>
</div>
@endsection