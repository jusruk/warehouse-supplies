@extends('layouts.manager') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Parduoti prekę {{$item->title}}</div>
				<div class="panel-body">
					@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
					@endif
					<div class="container">
						<h2>Kiekis</h2>
						<p>{{$item->quantity}}</p>
					</div>
					<div class="container">
						<h2>Vieneto kaina</h2>
						<p>{{$item->price}}</p>
					</div>
					<form class="form-horizontal" method="POST" action="{{route('confirm', ['id' => $item->id])}}">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="quantity" class="col-md-4 control-label">Parduodamas kiekis</label>

							<div class="col-md-6">
								<input id="quantity" type="text" class="form-control" name="quantity" value="{{old('quantity')}}" required autofocus>
							</div>
						</div>
						<div class="form-group">
							<label for="price" class="col-md-4 control-label">Pardavimo vieneto kaina</label>

							<div class="col-md-6">
								<input id="price" type="text" class="form-control" name="price" value="{{old('price')}}" required autofocus>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Parduoti
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection