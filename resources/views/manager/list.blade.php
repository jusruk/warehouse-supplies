@extends('layouts.manager')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            @if($items->count() > 0)
                <div class="panel-heading">Sandėlyje esančios prekės</div>
            @else
                <div class="panel-heading">Sandėlis tuščias</div>
            @endif
            @if($items->count() > 0)  
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                          
                    <table class="table table-striped">
                    <tr>
                            <th>Prekė</th>
                            <th>Kiekis</th>
                            <th>Vieneto kaina</th>
                            <th>Parduoti</th>
                        </tr>
                        @foreach($items as $item)
                            <tr>
                            <td>{{$item->title}}</td>
                            <td>{{$item->quantity}}</td>
                            <td>{{$item->price}}</td>
                            <td><a class="btn btn-default" href="{!!route('sell', ['id' => $item->id])!!}">Parduoti</a></td>
                            </tr>
                        @endforeach
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
