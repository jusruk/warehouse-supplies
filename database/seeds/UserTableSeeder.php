<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'administratorius')->first();
        $role_manager = Role::where('name', 'vadybininkas')->first();
        $role_stockman = Role::where('name', 'sandelininkas')->first();
        $role_supplier = Role::where('name', 'tiekejas')->first();

        $admin = new User();
        $admin->name = 'Administratorius';
        $admin->email = 'admin@sandelis.lt';
        $admin->password = bcrypt('admin');
        $admin->save();
        $admin->roles()->attach($role_admin);

        $manager = new User();
        $manager->name = 'Vadybininkas';
        $manager->email = 'management@sandelis.lt';
        $manager->password = bcrypt('manager');
        $manager->save();
        $manager->roles()->attach($role_manager);

        $stockman = new User();
        $stockman->name = 'Sandelininkas';
        $stockman->email = 'warehouse@sandelis.lt';
        $stockman->password = bcrypt('sandelis');
        $stockman->save();
        $stockman->roles()->attach($role_stockman);

        $supplier = new User();
        $supplier->name = 'Tiekejas';
        $supplier->email = 'supplies@sandelis.lt';
        $supplier->password = bcrypt('zaliavos');
        $supplier->save();
        $supplier->roles()->attach($role_supplier);
    }
}
