<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role();
        $role_admin->name = 'administratorius';
        $role_admin->description = 'sistemos administratorius: registruoja kitus vartotojus';
        $role_admin->save();
    
        $role_manager = new Role();
        $role_manager->name = 'vadybininkas';
        $role_manager->description = 'sandelio vadybininkas: parduoda prekes';
        $role_manager->save();

        $role_stockman = new Role();
        $role_stockman->name = 'sandelininkas';
        $role_stockman->description = 'sandelininkas: uzsako prekes is tiekeju';
        $role_stockman->save();

        $role_supplier = new Role();
        $role_supplier->name = 'tiekejas';
        $role_supplier->description = 'tiekejas: siulo prekes sandeliui';
        $role_supplier->save();
    }
}
