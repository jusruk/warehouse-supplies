<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home_path', 'uses' => function () {
    return view('welcome');
}]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin/user/register', 'UserController@register');
Route::post('/admin/user/register', 'UserController@registerUser');
Route::get('/admin/list', 'UserController@listUsers');
Route::get('/admin/traffic', 'UserController@items');
Route::get('/admin/offers-active', 'UserController@activeOffers');

Route::get('/manager/items', 'ManagerController@itemList');

Route::get('/supplier/offer', 'SupplierController@register');
Route::post('/supplier/offer', 'SupplierController@makeOffer');
Route::get('/supplier/list', 'SupplierController@list');
Route::get('/supplier/remove/{id}', [
    'uses' => 'SupplierController@remove',
    'as' => 'removeOffer'
]);

Route::get('/warehouse/stock', 'StockmanController@inStock');
Route::get('/warehouse/offers', 'StockmanController@offers');
Route::get('/warehouse/buy/{id}', [
    'uses' =>'StockmanController@acceptOffer',
    'as' => 'buy'
]);
Route::post('/warehouse/buy/{id}',[
    'uses' => 'StockmanController@buy',
    'as' => 'confirmBuy'
]);
Route::get('/warehouse/sell/{id}', [
    'uses' => 'ManagerController@sell',
    'as' => 'sell'
]);
Route::post('/warehouse/sell/{id}',[
    'uses' => 'ManagerController@confirm',
    'as' => 'confirm'
]);