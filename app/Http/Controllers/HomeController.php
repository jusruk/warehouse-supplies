<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->user()->hasRole('administratorius'))
            return view('admin-dashboard');
        if ($request->user()->hasRole('vadybininkas'))
            return view('manager-dashboard');
        if ($request->user()->hasRole('sandelininkas'))
            return view('stockman-dashboard');
        if ($request->user()->hasRole('tiekejas'))
            return view('supplier-dashboard');
    }
}
