<?php

namespace App\Http\Controllers;

use App\Item;
use App\Offer;
use App\Warehouse;
use Illuminate\Http\Request;

class StockmanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function inStock(Request $request)
    {
        if ($request->user()->hasRole('sandelininkas') === NULL)
            return redirect('/');
        $items = Item::where('location', '=', '1')->get();
        return view('stockman.stock', compact('items'));
    }

    public function offers(Request $request)
    {
        if ($request->user()->hasRole('sandelininkas') === NULL)
            return redirect('/');
        $offers = Offer::where('sum', '>', 0)->get();
        return view('stockman.offers', compact('offers'));
    }

    public function acceptOffer($id, Request $request)
    {
        if ($request->user()->hasRole('sandelininkas') === NULL) 
            return redirect('/');
        $offer = Offer::where('item_id', '=', $id)->first();
        $item = Item::where('id', '=', $id)->first();
        return view('stockman.buy', compact('offer', 'item'));
    }

    public function buy($id, Request $request)
    {
        if ($request->user()->hasRole('sandelininkas') === NULL)             
            return redirect('/');
        
        if (!is_numeric($_POST['quantity'])) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Įvestas kiekis turi būti skaičius!');
        }
        if ($_POST['quantity'] < 0) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Įvestas kiekis turi būti teigiamas skaičius!');
        }
        $offer = Offer::where('item_id', '=', $id)->first();
        $item = Item::where('id', '=', $id)->first();
        $warehouse = Warehouse::find(1);
        if ($_POST['quantity'] <= $item->quantity && $_POST['quantity'] <= $warehouse->capacity - $warehouse->count) {
            if ($_POST['quantity'] === $item->quantity) {
                $item->location = 1;
                $offer->sum = 0;
                $item->save();
                $offer->save();
            } else {
                $item_new = Item::create([
                    'title' => $item->title,
                    'price' => $item->price,
                    'quantity' => $_POST['quantity'],
                    'location' => 1,
                ]);
                $item->quantity -= $_POST['quantity'];
                $offer->sum = $item->quantity * $item->price;
                $item->save();
                $item_new->save();
                $offer->save();
            }
            $warehouse->count += $_POST['quantity'];
            $warehouse->save();
            return redirect()->back()->with('success', 'Prekė užsakyta!');
        } else {
            return redirect()->back()->withInput()->with('error', 'Negalite nusipirkti daugiau, nei siūloma!');
        }

    }
}
