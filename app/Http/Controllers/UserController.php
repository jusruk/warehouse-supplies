<?php

namespace App\Http\Controllers;

use App\Item;
use App\Offer;
use App\Role;
use App\User;
use App\Transaction;
use DB;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function register(Request $request)
    {
        if ($request->user()->hasRole('administratorius') === NULL)
            return redirect('/');
        return view('auth.register');
    }

    public function registerUser(Request $request)
    {
        if ($request->user()->hasRole('administratorius') === NULL)
            return redirect('/');
        $request->flash();
        if (User::where('email', '=', $_POST['email'])->exists()) {
            return redirect()->back()->withInput()->with('error', 'Toks vartotojas jau egzistuoja');
        }
        if (substr_count($_POST['email'], '@') !== 1) {
            return redirect()->back()->withInput()->with('error', 'Neteisingas elektronins paštas: turi būti tik 1 @ simbolis, rasta ' . substr_count($_POST['email'], '@'));
        }
        if (strlen($_POST['password']) < 5) {
            return redirect()->back()->withInput()->with('error', 'Slaptažodį turi sudaryti mažiausiai 5 simboliai');
        }

        $user = User::create([
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'password' => bcrypt($_POST['password']),
        ]);

        $user->roles()->attach(Role::where('name', $_POST['role'])->first());
        $user->save();
        return redirect()->action('UserController@register')->with('success', 'Vartotojas užregistruotas');
    }

    public function listUsers(Request $request)
    {
        if ($request->user()->hasRole('administratorius') === NULL)
            return redirect('/');
        $data = DB::table('role_user')->get();
        $users = array();
        foreach ($data as $pair) {
            $user_id = User::where('id', '=', $pair->user_id)->first()->id;
            $user_name = User::where('id', '=', $pair->user_id)->first()->name;
            $user_email = User::where('id', '=', $pair->user_id)->first()->email;
            $role = Role::where('id', '=', $pair->role_id)->first()->name;
            $users[] = array('id' => $user_id, 'name' => $user_name, 'email' => $user_email, 'role' => $role);
        }
        return view('admin.users', compact('users'));
    }

    public function items(Request $request)
    {
        if ($request->user()->hasRole('administratorius') === NULL)
            return redirect('/');
        $items = Item::all();
        $transactions = Transaction::all();
        $managers = array();
        foreach($transactions as $transaction){
            $item = $transaction->item_id;
            $manager_name = User::where('id', '=', $transaction->manager_id)->first()->name;
            $manager_email = User::where('id', '=', $transaction->manager_id)->first()->email;
            $managers[] = array('item' => $item, 'name' => $manager_name, 'email' => $manager_email);
        }
        return view('admin.items', compact('items', 'managers'));
    }

    public function activeOffers(Request $request)
    {
        if ($request->user()->hasRole('administratorius') === NULL)
            return redirect('/');
        $offers = Offer::where('sum', '>', 0)->get();
        $data = array();
        foreach ($offers as $offer) {
            $title = Item::where('id', '=', $offer->item_id)->first()->title;
            $quantity = Item::where('id', '=', $offer->item_id)->first()->quantity;
            $price = Item::where('id', '=', $offer->item_id)->first()->price;
            $data[] = array('title' => $title, 'quantity' => $quantity, 'price' => $price);
        }
        return view('admin.offers-active', compact('data'));
    }
}
