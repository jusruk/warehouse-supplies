<?php

namespace App\Http\Controllers;

use App\Item;
use App\Offer;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function register(Request $request)
    {
        if ($request->user()->hasRole('tiekejas') === NULL)
            return redirect('/');
        return view('supplier.offer');
    }

    public function makeOffer(Request $request)
    {
        
        if ($request->user()->hasRole('tiekejas') === NULL)
            return redirect('/');
        if (!is_numeric($_POST['quantity'])) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Įvestas kiekis turi būti skaičius!');
        }
        if ($_POST['quantity'] < 0) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Įvestas kiekis turi būti teigiamas skaičius!');
        }
        if (!is_numeric($_POST['price'])) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Įvestas kaina turi būti skaičius!');
        }
        if ($_POST['price'] < 0) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Įvestas kaina turi būti teigiamas skaičius!');
        }
        $check = Item::where('title', '=', strtolower($_POST['item']))->first();
        //jeigu preke yra
        if ($check !== null) {
            //jeigu preke yra aktyviame pasiulyme
            if ($check->location === 0) {
                $check->quantity += intval($_POST['quantity']);
                $check->price = doubleval($_POST['price']);
                $offer = Offer::where('item_id', '=', $check->id)->first();
                $offer->sum = $check->quantity * $check->price;
                $check->save();
                $offer->save();
            } else {
                $item = $check;
                $item->location = 0;
                $offer = Offer::create([
                    'item_id' => $item->id,
                    'sum' => $item->quantity * $item->price,
                ]);
                $item->save();
                $offer->save();
            }
        } else {
            $item = Item::create([
                'title' => strtolower($_POST['item']),
                'price' => doubleval($_POST['price']),
                'quantity' => intval($_POST['quantity']),
                'location' => 0,
            ]);
            $offer = Offer::create([
                'item_id' => $item->id,
                'sum' => $item->quantity * $item->price,
            ]);
            $item->save();
            $offer->save();
        }
        return redirect()->action('SupplierController@register')->with('success', 'Pasiūlymas pateiktas');
    }

    public function list(Request $request){
        if ($request->user()->hasRole('tiekejas') === NULL)
            return redirect('/');
        $offers = Offer::all();
        return view('supplier.list', compact('offers'));
    }

    public function remove($id, Request $request){
        if ($request->user()->hasRole('tiekejas') === NULL)
            return redirect('/');
        $offer = Offer::where('id', '=', $id)->first();
        $item = Item::where('id', '=', $offer->item_id)->delete();
        Offer::destroy($id);
        return redirect()->back()->with('success', 'Pasiūlymas pašalintas');
    }
}
