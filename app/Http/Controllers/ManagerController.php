<?php

namespace App\Http\Controllers;

use App\Item;
use App\Transaction;
use App\Warehouse;
use Illuminate\Http\Request;

class ManagerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function itemList(Request $request)
    {
        if ($request->user()->hasRole('vadybininkas') === NULL)
            return redirect('/');
        $items = Item::where('location', '=', '1')->get();
        return view('manager.list', compact('items'));
    }

    public function sell($id, Request $request)
    {
        if ($request->user()->hasRole('vadybininkas') === NULL)
            return redirect('/');
        $item = Item::where('id', '=', $id)->where('location', '=', 1)->first();
        return view('manager.sell', compact('item'));
    }
    public function confirm($id, Request $request)
    {
        if ($request->user()->hasRole('vadybininkas') === NULL)
            return redirect('/');
        if (!is_numeric($_POST['quantity'])) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Įvestas kiekis turi būti skaičius!');
        }
        if ($_POST['quantity'] < 0) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Įvestas kiekis turi būti teigiamas skaičius!');
        }
        if (!is_numeric($_POST['price'])) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Įvestas kaina turi būti skaičius!');
        }
        if ($_POST['price'] < 0) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Įvestas kaina turi būti teigiamas skaičius!');
        }
        $item = Item::where('id', '=', $id)->where('location', '=', 1)->first();
        if ($item !== null && $_POST['quantity'] > 0) {
            if ($item->price > $_POST['price']){
                $request->flash();
                return redirect()->back()->withInput()->with('error', 'Negalima parduoti prekės pigiau, nei ji buvo nupirkta!');
            }
            if ($item->quantity === $_POST['quantity']) {
                $item->location = 2;
                $item->price = $_POST['price'];
                $transaction = Transaction::create([
                    'manager_id' => $request->user()->id,
                    'item_id' => $id,
                    'sum' => $item->price * $item->quantity,
                ]);
                $item->save();
                $transaction->save();
            } else if ($_POST['quantity'] < $item->quantity) {
                $item_new = Item::create([
                    'title' => $item->title,
                    'price' => $_POST['price'],
                    'quantity' => $_POST['quantity'],
                    'location' => 2,
                ]);
                $item_new->save();
                $item_new = Item::where('title', '=', $item->title)->where('location', '=', 2)->orderBy('created_at', 'DESC')->first();
                $item->quantity -= $_POST['quantity'];
                $item->save();
                $transaction = Transaction::create([
                    'manager_id' => $request->user()->id,
                    'item_id' => $item_new->id,
                    'sum' => $item_new->price * $item_new->quantity,
                ]);

                $transaction->save();
            } else {
                return redirect()->back()->withInput()->with('error', 'Įvestas kiekis turi būti mžesnis arba lygus esamam kiekiui!');
            }
            $warehouse = Warehouse::find(1);
            $warehouse->count -= $_POST['quantity'];
            $warehouse->save();
            return redirect()->action('ManagerController@itemList')->with('success', 'Prekė parduota');
        }
    }
}
